# Solr

[TOC]

## Solr导入Mysql数据库

### 1	新建core

新建一个core，如果找不到配置文件就把configsets里的样例conf拷过去

### 2	添加jar

solr/webapp/WEB_INF/lib   

或者

core/lib

添加数据库驱动包，数据导入包

mysql-connector-java-xxx.jar

solr-dataimporthandler-xxx.jar

solr-dataimporthandler-extras-xxx.jar

### 3	新建data_config.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<dataConfig>
    <dataSource name="jdbcDataSource"  type="JdbcDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/库名?useUnicode=true&amp;characterEncoding=utf-8" user="root" password="123456" />
    <document>
        <entity dataSource="jdbcDataSource" name="表名"  pk="主键"   query="SQL语句">
        <field column="id" name="id" />
	    <field column="name" name="name" />
        </entity>
    </document>
</dataConfig>
```

说明： field区域当中的column对应数据库中的列，而name就是solr中显示的名称。

### 4	修改solrconfig.xml文件

```xml
<requestHandlername="/dataimport"class="org.apache.solr.handler.dataimport.DataImportHandler">  
         <lst name="defaults">  
               <strname="config">data-config.xml</str>  
         </lst>  
</requestHandler>
```

### 5	修改schema.xml（managed-schema）文件

schema.xml这个配置文件的目的是为了通过配置告诉Solr如何建立索引

schema.xml的基本结构:

```xml
<schema>
    <field>
	<dynamicField>
    <copyField>
	<uniqueKey>
	<fieldType>
</schema>
```

- field：定义一个document中的各个fields

  - name：必填。该field的名字。前后都有下划线的name是系统保留的名字，比如“_version_”
  - type：必填。类型，对应于fieldType的name
  - default：该field的缺省值
  - indexed：true/false，是否为该field建立索引，以让用户可以搜索它、统计它（facet）
  - stored：true/false，定义这个field是否可以返回给查询者
  - multiValued：true/false，是否可以容纳多个值（比如多个copyField的dest指向它）。如果是true，则该field不能被排序、不能作为uniqueKey
  - required：true/false，告诉solr这个field是否接受空值，缺省为false
  - docValues：true/false，建立document-to-value索引，以提高某些特殊搜索的效率（排序、统计、高亮）

- dynamicField

  - 用通配符定义一个field来存在没有被field定义的name：使用通配符，比如“*_i”，来处理类似“xxx_i”之类的field

- uniqueKey：指定一个field为唯一索引

- copyField：把一个field的内容拷贝到另外一个field中。一般用来把几个不同的field copy到同一个field中，以方便只对一个field进行搜索

  - source：被拷贝的field，支持用通配符指定多个field，比如：*_name
  - dest：拷贝到的目的field
  - maxChars：最大字符数

- fieldType：定义field的类型，包括下面一些属性

  - name：必填，被field配置使用
  - class：必填，filedType的实现类。solr.TextField是路径缩写，"等价于"org.apache.solr.schema.TextField"
  - multiValued：？
  - positionIncrementGap：指定mutiValued的距离
  - ananlyzer：如果class是solr.TextField，这个配置是必填的。告诉solr如何处理某些单词、如何分词，比如要不要去掉“a”，要不要全部变成小写……
  - type：index或query
  - tokenizer：分词器，比如：StandardTokenizerFactory
  - filter：过滤器，比如：LowerCaseFilterFactory

  

  

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<schema name="course_video" version="1.5">
        <field name="id" type="int" indexed="true" stored="true" required="true" multiValued="false" />
        <field name="_version_" type="long" indexed="true" stored="true"/>
        <field name="title" type="string" indexed="true" stored="true" required="true" multiValued="false" />
        <field name="tags" type="string" indexed="true" stored="false" required="false" multiValued="false" />
        <field name="content" type="string" indexed="true" stored="true" required="false" multiValued="false" />
        <field name="info_text" type="text_general" indexed="true" stored="false" multiValued="true" />
 
        <copyField source="title" dest="info_text" />
        <copyField source="content" dest="info_text" />
        <copyField source="tags" dest="info_text" />
 
        <uniqueKey>id</uniqueKey>
 
        <fieldType name="string" class="solr.StrField" sortMissingLast="true" />
        <fieldType name="int" class="solr.TrieIntField" precisionStep="0" positionIncrementGap="0"/> 
 
    <fieldType name="text_general" class="solr.TextField" positionIncrementGap="100">
      <analyzer type="index">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
      </analyzer>
 
      <analyzer type="query">
        <tokenizer class="solr.StandardTokenizerFactory"/>
        <filter class="solr.LowerCaseFilterFactory"/>
      </analyzer>
    </fieldType>
 
</schema>

```

### 6	重启solr



### 7	 导入

选择Dataimport

Solr提供了full-import和delta-import两种导入方式

full-import：

delta-import:



