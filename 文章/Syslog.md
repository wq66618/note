# Rsyslog+MySQL部署日志服务器

默认系统都安装了rsyslog

## 配置服务端

### 安装rsyslog连接MySQL数据库的模块

rsyslog使用此模块将数据传入MySQL数据库，必须安装

```bash
$  yum -y install rsyslog-mysql
```

安装模块后创建了Syslog 库并在该库中创建了两张空表SystemEvents 和SystemEventsProperties

### 支持rsyslog-mysql模块，并开启UDP服务端口

```bash
$  vim  /etc/rsyslog.conf
```

 在  

#################

​          MODULES

#################  

下添加

```
*.* :ommysql:localhost,Syslog,root,kean123456
```

注:localhost表示本地主机，Syslog为数据库名，root为数据库的用户，kean123456为该用户密码

```
module(load="immark")   # immark是模块名，支持日志标记
# provides UDP syslog reception
module(load="imudp")    #imupd是模块名，支持udp协议
input(type="imudp" port="514")  #允许514端口接收使用UDP协议转发过来的日志

# provides TCP syslog reception
module(load="imtcp")     #imupd是模块名，支持tcp协议
input(type="imtcp" port="514")  #允许514端口接收使用TCP协议转发过来的日志
```

配置日志文件记录

```
#$template logFormat, "%rawmsg%\n"$template DynaFile, "/var/log/%$YEAR%%$MONTH%%$DAY%/%fromhost-ip%-%$YEAR%-%$MONTH%-%$DAY%.log"$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat#$ActionFileDefaultTemplate logFormat:fromhost-ip, !isequal, "127.0.0.1" -?DynaFile;RSYSLOG_TraditionalFileFormat
```

/var/log/messages不写入local4.none

```
*.info;mail.none;authpriv.none;cron.none;local4.none /var/log/messages
```

最终配置，不搜集本机日志

```
$Modload ommysql

$template MySQLInsert,"insert into SystemEvents (Message, Facility, FromHost,Priority, DeviceReportedTime, ReceivedAt, InfoUnitID, SysLogTag,processid) values (
'%msg%', %syslogfacility%, '%HOSTNAME%', %syslogpriority%, '%timereported:::date-mysql%', '%timegenerated:::date-mysql%', %iut%, '%syslogtag%', '%fromhost-ip%')
",SQL
:fromhost-ip, !isequal, "127.0.0.1" :ommysql:localhost,Syslog,root,kean123456;MySQLInsert
#$template logFormat, "%rawmsg%\n"
$template DynaFile, "/var/log/%$YEAR%%$MONTH%%$DAY%/%fromhost-ip%-%$YEAR%-%$MONTH%-%$DAY%.log"
$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat
#$ActionFileDefaultTemplate logFormat
:fromhost-ip, !isequal, "127.0.0.1" -?DynaFile;RSYSLOG_TraditionalFileForma
:fromhost-ip, !isequal, "127.0.0.1" ～ #后面忽略
#&~ #全部不做后续处理
*.info;mail.none;authpriv.none;cron.none;local0.none；local1.none；local2.none；local3.none；local4.none；local5.none；local6.none；local7.none /var/log/messages
```

重启rsyslog服务

```bash
$ service syslog restart
```

## 配置客户端

编辑/etc/bash.bashrc，将客户端执行的所有命令写入系统日志/var/log/messages中

 在结尾处加上此内容

```
# vim /etc/bashrc
export PROMPT_COMMAND='{ msg=$(history 1 | { read x y; echo $y; });logger "[euid=$(whoami)]":$(who am i):[`pwd`]"$msg"; }'
```

配置rsyslog客户端发送本地日志到服务端

```bash
$ vim /etc/rsyslog.conf
```

```
在末行添加一行：

*.* @日志服务器端IP地址
```

重启rsyslog服务

```bash
$ service syslog restart
```

