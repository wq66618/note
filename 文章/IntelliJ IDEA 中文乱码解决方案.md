# IntelliJ IDEA 中文乱码解决方案

[TOC]

## 1、编辑器乱码

file->settings->appearence

Name选项设置成支持中文的字体

## 2、控制台乱码

#### 1、 配置Intellij的配置文件

- idea安装目录下的bin   idea.exe.vmoptions和idea64.exe.vmoptions文件

		在文件中添加：   -Dfile.encoding=UTF-8

- HELP->Edit Custom VM OPtions

		在文件中添加：   -Dfile.encoding=UTF-8

#### 2、 配置项目编码及IDE编码

		选择Settings-Editor-File Encodings，
	
		Global Encoding和Project Encoding配置为UTF-8
	
		Default encoding for properties files配置为UTF-8	

#### 3、 配置项目启动服务器参数

		打开Run-Edit Configurations 选择你的tomcat
	
		VM options 项中添加：   -Dfile.encoding=UTF-8

#### 4、 重启IntelliJ IDEA



## 3、 尝试上面方法后依旧不好使的解决办法

- 清空build产生的文件   重新rebuild一下

- 项目目录下的.idea的文件夹，encodings.xml的文件，把encodings.xml里面的除了UTF-8的都删掉

  