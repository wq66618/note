## solo 小而美的博客系统，为未来而构建

环境Centos7.4 root用户

### 安装Java

```bash
$ yum -y list java*

$ yum -y install java-1.8.0-openjdk*

$ java -version
```

### 安装Mysql

安装mysql源

```
$ wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm

$ rpm -ivh mysql-community-release-el7-5.noarch.rpm

$ yum install mysql-server
如果报错

$ yum install glibc.i686

$ yum list libstdc++*
```

mysql -u root

```
如果报错

$ chown -R root:root /var/lib/mysql
```

**重启服务**

```
$ service mysqld restart
```

**登录重置密码**

```
$ mysql -u root -p

mysql > use mysql;

mysql > update user set password=password('') where user='root';

mysql > exit;
```

**数据库编码格式**

```
mysql > show variables like"%char%";

mysql > setnames utf8;
```

**为root添加远程连接**

```
mysql>GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '您的数据库密码' WITH GRANT OPTION;

mysql>flush privileges;
```

### 安装Nginx

安装yum 源

```
$ rpm -ivh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm

$ yum repolist

$ yum install nginx
```

设置开机启动

```
$ sudo systemctl enable nginx
```

启动服务

```
$ sudo systemctl start nginx
```

停止服务

```
$ sudo systemctl restart nginx
```

重新加载，因为一般重新配置之后，不希望重启服务，这时可以使用重新加载。

```
$ sudo systemctl reload nginx
```

**反向代理**

CentOS 7 的 SELinux，使用反向代理需要打开网络访问权限。

```
$ sudo setsebool httpd_can_network_connect 1 
```

绑定其他端口

Nginx 默认绑定的端口是 http 协议的默认端口，端口号为：`80`，如果需要绑定其他端口，需要注意 SELinux 的配置

例如：绑定 8081 端口，但是会发现无法启动，一般的报错如下

```
YYYY/MM/DD hh:mm:ss [emerg] 46123#0: bind() to 0.0.0.0:8081 failed (13: Permission denied)
```

此时需要更改 SELinux 的设置。我们使用 SELinux 的管理工具 `semanage` 进行操作，比较方便。

安装 `semanage` 使用如下命令

```
$ sudo yum install policycoreutils-python
```

然后查看是否有其他协议类型使用了此端口

```
$ sudo semanage port -l | grep 8081
transproxy_port_t              tcp      8081
```

返回了结果，表明已经被其他类型占用了，类型为 `transproxy_port_t`。

我们还要查看一下 Nginx 的在 SELinux 中的类型 `http_port_t` 绑定的端口

```
$ sudo semanage port -l | grep http_port_t
http_port_t                    tcp      80, 81, 443, 488, 8008, 8009, 8443, 9000
pegasus_http_port_t            tcp      5988
```

第一行 `http_port_t` 中没有包含 `8081` 这个端口。因此需要修改 `8081` 端口到 `http_port_t` 类型中。

```
$ sudo semanage port -m -p tcp -t http_port_t 8081
```

如果没有其他协议类型使用想要绑定的端口，如 `8001`，则我们只要新增到 SELinux 中即可。

```
$ sudo semanage port -l | grep 8001
$ sudo semanage port -a -p tcp -t http_port_t 8001
```

重新启动 Nginx

### Docker 获取最新solo镜像

```shell
docker pull b3log/solo
```

#### 启动容器

- 使用 MySQL

  先手动建库（库名 `solo`，字符集使用 `utf8mb4`，排序规则 `utf8mb4_general_ci`），然后启动容器：

  ```shell
  docker run --detach --name solo --network=host \
      --env RUNTIME_DB="MYSQL" \
      --env JDBC_USERNAME="root" \
      --env JDBC_PASSWORD="123456" \
      --env JDBC_DRIVER="com.mysql.cj.jdbc.Driver" \
      --env JDBC_URL="jdbc:mysql://127.0.0.1:3306/solo?useUnicode=yes&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC" \
      b3log/solo --listen_port=8080 --server_scheme=http --server_host=localhost --server_port=
  ```

  为了简单，使用了主机网络模式来连接主机上的 MySQL。

启动参数说明：

- `--listen_port`：进程监听端口
- `--server_scheme`：最终访问协议，如果反代服务启用了 HTTPS 这里也需要改为 `https`
- `--server_host`：最终访问域名或公网 IP，不要带端口
- `--server_port`：最终访问端口，使用浏览器默认的 80 或者 443 的话值留空即可

完整启动参数的说明可以使用 `-h` 来查看。

#### 日志配置

默认通过 log4j 将日志打印到标准输出流，可以通过 `docker logs solo` 进行查看。如果需要覆盖 log4j 配置，可通过挂载文件实现：

```shell
--volume ~/log4j.properties:/opt/solo/WEB-INF/classes/log4j.properties
```

#### 皮肤配置

如果要使用其他皮肤，可以挂载目录 skins（里面需要包含所需使用的所有皮肤，官方所有皮肤可从[这里](https://hacpai.com/forward?goto=https%3A%2F%2Fgithub.com%2Fb3log%2Fsolo-skins)下载）：

```shell
--volume /root/skins/:/opt/solo/skins/
```

#### 文章导入

```
--volume 待导入目录/:/opt/solo/markdowns/
```

#### 版本升级

1. 拉取最新镜像
2. 重启容器

```bash
docker pull b3log/solo
docker stop solo
docker rm solo
docker run --detach --name solo --network=host \
    --env RUNTIME_DB="MYSQL" \
    --env JDBC_USERNAME="root" \
    --env JDBC_PASSWORD="" \
    --env JDBC_DRIVER="com.mysql.cj.jdbc.Driver" \
    --env JDBC_URL="jdbc:mysql://127.0.0.1:3306/solo?useUnicode=yes&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC" \
	--volume /root/skins/:/opt/solo/skins/ \
    b3log/solo --listen_port=80 --server_scheme=http --server_host=xxxxxxx --server_port=
```

#### 进入容器

```
docker exec -it solo /bin/sh
```

#### NGINX 反代

```
upstream backend {
    server localhost:8080; # Solo 监听端口
}

server {
    listen       80;
    server_name  88250.b3log.org; # 博客域名

    access_log off;

    location / {
        proxy_pass http://backend$request_uri;
        proxy_set_header  Host $host:$server_port;
        proxy_set_header  X-Real-IP  $remote_addr;
        client_max_body_size  10m;
    }
}
upstream backend {
    server localhost:8080; # Solo 监听端口
}

server {
    listen       80;
    server_name  88250.b3log.org; # 博客域名

    access_log off;

    location / {
        proxy_pass http://backend$request_uri;
        proxy_set_header  Host $host:$server_port;
        proxy_set_header  X-Real-IP  $remote_addr;
        client_max_body_size  10m;
    }
}
```

[Solo 官方文档](https://hacpai.com/article/1492881378588)

[Solo 皮肤开发指南](https://hacpai.com/article/1493814851007)