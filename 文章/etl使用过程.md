## Kettle第一次使用教程

### 开始使用

1. 在使用软件前要配置好java环境变量,并且需要数据库连接驱动包  

   在\data-integration\lib\下 添加mysql-connector-java-xxxxxx.jar和ojdbc14-xxxxxx.jar

2. 双击\data-integration\的 data-integration 的Spoon.bat,就可以打开界面

### 新建转换 ![mark](http://img.hugqq.com/img/20190815/MnVCTuoHyRdx.png)

   

 	右键新建   ![mark](http://img.hugqq.com/img/20190815/R4DMKQQTqPht.png)

   

 	点击主对象树		![mark](http://img.hugqq.com/img/20190815/Rrq4p9gkRH4q.png)



 	在DB连接  右键新建![mark](http://img.hugqq.com/img/20190815/5uXPYh8SSJIg.png)



  mysql配置  根据所需进行相应的更改

![mark](http://img.hugqq.com/img/20190815/HJVgMoIdVnB3.png)

   oracle配置   根据所需进行相应的更改![mark](http://img.hugqq.com/img/20190815/fGQSfn0BIVJJ.png)	

   创建完记得测试下

![mark](http://img.hugqq.com/img/20190815/dkMRTxSI4phb.png)

### 开始转换

点击 核心对象-输入,在最下面有个 表输入,鼠标左键按住拉入到空白区;

![1565866424937](C:\Users\zpz\AppData\Roaming\Typora\typora-user-images\1565866424937.png)

再点击 输出,和表输入一样,搞定表输出

![1565866466058](C:\Users\zpz\AppData\Roaming\Typora\typora-user-images\1565866466058.png)



按住shift 拖动鼠标连接三者,按顺序连接

![mark](http://img.hugqq.com/img/20190815/9Ce2xn8TkLth.png)

7.双击表输入

我们是从mysql导入到oracle

数据库连接选择mysql

![mark](http://img.hugqq.com/img/20190815/0xHecmviOcCR.png)

双击选择要导入的表

![mark](http://img.hugqq.com/img/20190815/miCHpQbR6uLu.png)



再双击表输出

![1565866620209](C:\Users\zpz\AppData\Roaming\Typora\typora-user-images\1565866620209.png)

目标表要手打  因为oracle表太多 浏览会卡死

指定数据库字段记得勾上



点击数据库字段,选择获取字段  (**注意:字段匹配一定要一样,不能多不能少,对应名字无所谓,但是要对应属性**)

![mark](http://img.hugqq.com/img/20190815/IXEcwsUBUUG9.png)



ctrl+s 保存 ktr文件  并命名 方便下次使用



### 新建作业

![mark](http://img.hugqq.com/img/20190815/5Da0eYAFVITk.png)

双击Start 可以设置定时  等等

双击转换

![mark](http://img.hugqq.com/img/20190815/znKWSNBu74mK.png)

导入保存的ktr文件,确定

执行

![mark](http://img.hugqq.com/img/20190815/reakY7CqS366.png)



## 在已有ktr kjb情况下

![mark](http://img.hugqq.com/img/20190815/jF8F3TfuChSf.png)

文件打开ktr文件 或者 kjb文件进行所需编辑

