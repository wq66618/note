[begin]安[/begin]装wamp环境的坑还是比较多的,总结一下

[TOC]
OS: Windows 10

### 前置条件

需要安装Windows Visual C++ VC14+以上的版本安装包。 Visual Studio 2015, 2017 and 2019
下载地址： https://support.microsoft.com/en-hk/help/2977003/the-latest-supported-visual-c-downloads 

### 下载 apache

下载地址：http://httpd.apache.org/docs/current/platform/windows.html 
![mark](http://img.hugqq.com/img/20190803/BhzjI0botz5s.png?imageslim)

Apache本身不提供Windows下的安装包,提供了几个第三方提供的binary安装包 ,我选择了Apache Haus 

![mark](http://img.hugqq.com/img/20190803/qJ5IPNUz8sop.png?imageslim)

### 下载php7

php版本提供了non-thread-safe和thread-safe两个版本，下载thread-safe的版本： 
下载地址：https://windows.php.net/download

![mark](http://img.hugqq.com/img/20190803/5miHGz29uRvi.png?imageslim)

### 安装目录的设置

将php7.1以及解压之后的apache 2.4放置到了特定目录：   我放到了 D:\php

### 配置Php

进入php的目录，将php.ini-production文件复制一份，重新命名为php.ini

设置环境变量:

![mark](http://img.hugqq.com/img/20190803/wUHPvb8aBLR4.png?imageslim)

### 配置Apache

打开apache安装目录下的conf，编辑httpd.conf, 将其中的SRVROOT修改为实际的安装目录，下面为从中截取的片段：

```
Define SRVROOT "D:\php\Apache24"
ServerRoot “${SRVROOT}”
```

这里着重讲SRVROOT替换为实际的Apache的安装目录。

其他的修改包括：

```
 <IfModule dir_module>
    DirectoryIndex index.html index.php index.htm
</IfModule>
```

新增index.php来支持php的版本

### 安装Apache服务

管理员身份运行windows下的cmd， 然后安装apache服务：

```
D:\php\Apache24\bin\httpd.exe -k install -n apache
```

这里可能提示端口占用

解决办法

一、修改端口号：

1、443端口被占用，apache无法监听443端口

在/xampp/apache/conf/extra/httpd-ssl.conf 

把Listen 443 修改为 444（可自定义）

2、80端口被占用，apache无法监听80端口 

在/xampp/apache/conf/extra/httpd.conf 

把Listen 80 修改为 88 （可自定义）

如果配置了vhosts的话请把httpd-vhosts.conf 中端口改为88（同上端口号）

二、最直接的方法是关闭占用80、443端口的进程： 

1、通过cmd中netstat -ano (netstat -a -o)看看本机80、 443端口被占用没    这里可能会被其他程序占用如iis、虚拟机等

2、通过cmd中打印tasklist，查找占用80、443端口的进程名称。 

3、taskkill /F /PID  端口号 杀掉此进程名称，重启apache即可



### 配置php.ini

**配置php的php.ini文件：**

```
; Directory in which the loadable extensions (modules) reside.
; http://php.net/extension-dir
 extension_dir = "D:/php/php-7.1.31-Win32-VC14-x64"
 On windows:
 extension_dir = "D:/php/php-7.1.31-Win32-VC14-x64/ext"
```

**打开访问dll**

```
 ;extension=php_curl.dll 去掉前面的分号
 ;extension=php_gd2.dll 去掉前面的分号
 ;extension=php_mbstring.dll 去掉前面的分号
 ;extension=php_mysqli.dll 去掉前面的分号
 ;extension=php_pdo_mysql.dll 去掉前面的分号
```

**配置Apache的httpd.conf**
在之前添加如下信息： 

```
LoadModule php7_module "D:/php/php-7.1.31-Win32-VC14-x64/php7apache2_4.dll"
PHPIniDir "D:/php/php-7.1.31-Win32-VC14-x64"
AddType application/x-compress .Z
AddType application/x-gzip .gz .tgz
AddType application/x-httpd-php .php
AddType application/x-httpd-php .html
AddType application/pdf .pdf
```

### 启动Apache服务

进入Apache的安装目录，运行ApacheMonitor.exe

### apache的错误信息查看

apache启动错误查询。右键点击“计算机”》打开“管理”》“事件查看器”》“windows日志”》“应用程序”即可查询报错内容

### 测试php页面
创建一个php页面：
``` php
<?php
phpinfo();
?>
```

